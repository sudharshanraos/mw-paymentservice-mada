package com.tarang.middleware.paymentservice.enums;

import lombok.Getter;

/**
 * The Enum TransactionTypes.
 * 
 * @author sudharshan.s
 */
@Getter
public enum TransactionTypes {

    ADMIN_NOTIFICATION("ADMIN_NOTIFICATION", "1644", "1654"), //

    AUTH("AUTH", "1100", "1110"), //
    AUTH_ADVISE("AUTH_ADVISE", "1120", "1130"), //
    AUTH_ADVISE_REPEAT("AUTH_ADVISE_REPEAT", "1121", "1131"), //

    ECHO("ECHO", "0800", "0810"), //

    FINANCIAL("FINANCIAL", "1200", "1210"), //
    FINANCIAL_ADVISE("FINANCIAL_ADVISE", "1220", "1230"), //
    FINANCIAL_ADVISE_REPEAT("FINANCIAL_ADVISE_REPEAT", "1221", "1231"), //

    FILE_ACTION("FILE_ACTION", "1304", "1314"), //
    FILE_ACTION_REPEAT("FILE_ACTION_REPEAT", "1305", "1315"), //

    NETWORK_MNGMT("NETWORK_MNGMT", "1804", "1814"), //

    OFFLINE_PURCHASE("OFFLINE_PURCHASE", "1220", "1230"), //

    PRE_AUTH("PRE_AUTH", "1100", "1110"), //
    PRE_AUTH_VOID("PRE_AUTH_VOID", "1120", "1130"), //
    PRE_AUTH_VOID_FULL("PRE_AUTH_VOID_FULL", "1120", "1130"), //
    PRE_AUTH_VOID_PARTIAL("PRE_AUTH_VOID_PARTIAL", "1120", "1130"), //
    PRE_AUTH_EXTENSTION("PRE_AUTH_EXTENSTION", "1120", "1130"), //
    PRE_AUTH_COMPLETE("PRE_AUTH_COMPLETE", "1120", "1130"), //

    PURCHASE("PURCHASE", "1200", "1210"), //
    PURCHASE_ADVICE("PURCHASE_ADVICE", "1220", "1230"), //
    PURCHASE_ADVICE_FULL("PURCHASE_ADVICE_FULL", "1220", "1230"), //
    PURCHASE_ADVICE_PARTIAL("PURCHASE_ADVICE_PARTIAL", "1220", "1230"), //
    PURCHASE_ADVICE_MANUAL("PURCHASE_ADVICE_MANUAL", "1220", "1230"), //
    PURCHASE_REVERSAL("PURCHASE_REVERSAL", "1420", "1430"), //

    RECONCILIATION("RECONCILIATION", "1524", "1534"), //
    RECONCILIATION_REPEAT("RECONCILIATION_REPEAT", "1525", "1535"), //

    REFUND("REFUND", "1420", "1430"), //

    REVERSAL("REVERSAL", "1420", "1430"), //
    REVERSAL_REPEAT("REVERSAL_REPEAT", "1421", "1431"), //

    TERMINAL_REGISTRATION("TERMINAL_REGISTRATION", "1804", "1814"),//

    ;

    /** The type. */
    private final String type;

    /** The req MTI. */
    private final String reqMTI;

    /** The res MTI. */
    private final String resMTI;

    /**
     * Instantiates a new transaction types.
     *
     * @param type
     *            the type
     * @param reqMTI
     *            the req MTI
     * @param resMTI
     *            the res MTI
     */
    TransactionTypes(String type, String reqMTI, String resMTI) {
        this.type = type;
        this.reqMTI = reqMTI;
        this.resMTI = resMTI;
    }

    /**
     * Gets the transaction types.
     *
     * @param type
     *            the type
     * @return the transaction types
     */
    public static TransactionTypes getTransactionTypes(String type) {
        for (TransactionTypes v : TransactionTypes.values()) {
            if (v.name().equalsIgnoreCase(type)) {
                return v;
            }
        }
        return FINANCIAL;
    }
}
