package com.tarang.middleware.paymentservice.enums;

import lombok.Getter;

/**
 * APi Details.
 * 
 * @author sudharshan.s
 */
@Getter
public enum ApiDetails {

    /** The payment service management. */
    PAYMENT_SERVICE_MANAGEMENT("Payment Service Management", "Payment Service Management API Services"),

    /** The system config management. */
    SYSTEM_CONFIG_MANAGEMENT("System Configuration Management", "System Configuration Management API Services"),

    ;

    /** The key. */
    private final String key;

    /** The value. */
    private final String value;

    /**
     * A mapping between the integer code and its corresponding text to
     * facilitate lookup by code.
     *
     * @param key
     *            the key
     * @param value
     *            the value
     */

    ApiDetails(String key, String value) {
        this.value = value;
        this.key = key;
    }
}
