package com.tarang.middleware.paymentservice.enums;

import lombok.Getter;

// TODO: Auto-generated Javadoc
/**
 * Gets the description.
 *
 * @return the description
 */
@Getter
public enum ResponseCodes {
	
    /** The rc 000. */
    RC_000("000","Approved"),

    /** The rc 001. */
    RC_001("001","Honour with identification"),
    
    /** The rc 003. */
    RC_003("003","Approved (VIP)"),
    
    /** The rc 007. */
    RC_007("007","Approved, update ICC"),
    
    /** The rc 060. */
    RC_060("060","Approved in Stand-In Processing (STIP) mode by mada switch"),
    
    /** The rc 061. */
    RC_061("061","Approved in Stand-In Processing (STIP) mode by mada POS"),
    
    /** The rc 087. */
    RC_087("087","Offline Approved (Chip only)"),
    
    /** The rc 089. */
    RC_089("089","Unable to go On-line. Off-line approved (Chip only)"),
    
    /** The rc 100. */
    RC_100("100", "Do not honour"),
    
    /** The rc 101. */
    RC_101("101", "Expired card"),
    
    /** The rc 102. */
    RC_102("102", "Suspected fraud (To be used when ARQC validation fails)"),
    
    /** The rc 103. */
    RC_103("103", "Card acceptor contact acquirer"),
    
    /** The rc 104. */
    RC_104("104", "Restricted card"),
    
    /** The rc 105. */
    RC_105("105", "Card acceptor call acquirer’s security department"),
    
    /** The rc 106. */
    RC_106("106", "Allowable PIN tries exceeded"),
    
    /** The rc 107. */
    RC_107("107", "Refer to card issuer"),
    
    /** The rc 108. */
    RC_108("108", "Refer to card issuer’s special conditions"),
    
    /** The rc 109. */
    RC_109("109", "Invalid merchant"),
    
    /** The rc 110. */
    RC_110("110", "Invalid amount"),
    
    /** The rc 111. */
    RC_111("111", "Invalid card number"),
    
    /** The rc 112. */
    RC_112("112", "PIN data required"),
    
    /** The rc 114. */
    RC_114("114", "No account of type requested"),
    
    /** The rc 115. */
    RC_115("115", "Requested function not supported"),
    
    /** The rc 116. */
    RC_116("116", "Not sufficient funds"),
    
    /** The rc 117. */
    RC_117("117", "Incorrect PIN"),
    
    /** The rc 118. */
    RC_118("118", "No card record"),
    
    /** The rc 119. */
    RC_119("119", "Transaction not permitted to cardholder"),
    
    /** The rc 120. */
    RC_120("120", "Transaction not permitted to terminal"),
    
    /** The rc 121. */
    RC_121("121", "Exceeds withdrawal amount limit"),
    
    /** The rc 122. */
    RC_122("122", "Security violation"),
    
    /** The rc 123. */
    RC_123("123", "Exceeds withdrawal frequency limit"),
    
    /** The rc 125. */
    RC_125("125", "Card not effective"),
    
    /** The rc 126. */
    RC_126("126", "Invalid PIN block"),
	
	/** The rc 127. */
	RC_127("127", "PIN length error"),
	
	/** The rc 128. */
	RC_128("128", "PIN key synch error"),
	
	/** The rc 129. */
	RC_129("129", "Suspected counterfeit card"),
	
	/** The rc 164. */
	RC_164("164", "Transaction not permitted for this Merchant Category Code (MCC)"),
	
	/** The rc 165. */
	RC_165("165", "Excessive capture not allowed"),
	
	/** The rc 166. */
	RC_166("166", "More than one pre-authorization extension is not allowed"),
	
	/** The rc 167. */
	RC_167("167", "Issuer not certified for this transaction or service or Issuer does not support this service"),
	
	/** The rc 182. */
	RC_182("182", "Invalid date (Visa 80)"),
	
	/** The rc 183. */
	RC_183("183", "Cryptographic error found in PIN or CVV (Visa 81)"),
	
	/** The rc 184. */
	RC_184("184", "Incorrect CVV (Visa 82)"),
	
	/** The rc 185. */
	RC_185("185", "Unable to verify PIN (Visa 83)"),
	
	/** The rc 187. */
	RC_187("187", "Original transaction for refund, preauthorization completion, preauthorization void or preauthorization extension not found based on original transaction data."),
	
	/** The rc 188. */
	RC_188("188", "Offline declined"),
	
	/** The rc 190. */
	RC_190("190", "Unable to go online – Offline declined"),
	
	/** The rc 195. */
	RC_195("195", "Individual transaction amount exceeds limit"),
	
	/** The rc 196. */
	RC_196("196", "Cumulative contactless limit exceeded"),
	
	/** The rc 197. */
	RC_197("197", "The bank account used in the original transaction does not match the bank account being used in the Refund, Preauthorization Completion, reauthorization Void or Preauthorization Extension transaction"),
	
	/** The rc 198. */
	RC_198("198", "The refund, preauthorization void amount or cumulative amount exceeds the original transaction amount"),
	
	/** The rc 199. */
	RC_199("199", "The refund, pre-authorization completion, preauthorization void or preauthorization extension transaction period exceeds the maximum time limit allowed by the mada business rules"),
	
	/** The rc 200. */
	RC_200("200", "Do not honour"),
   
	/** The rc 201. */
	RC_201("201", "Expired card"),
	
	/** The rc 202. */
	RC_202("202", "Suspected fraud (To be used when ARQC validation fails)"),
	
	/** The rc 203. */
	RC_203("203", "Card acceptor contact acquirer"),
	
	/** The rc 204. */
	RC_204("204", "Restricted card"),
	
	/** The rc 205. */
	RC_205("205", "Card acceptor call acquirer’s security department"),
	
	/** The rc 206. */
	RC_206("206", "Allowable PIN tries exceeded"),
	
	/** The rc 207. */
	RC_207("207", "Special conditions"),
	
	/** The rc 208. */
	RC_208("208", "Lost card"),
	
	/** The rc 209. */
	RC_209("209", "Stolen card"),
	
	/** The rc 210. */
	RC_210("210", "Suspected counterfeit card"),
	
	/** The rc 300. */
	RC_300("300", "Successful"),
	
	/** The rc 301. */
	RC_301("301", "Not supported by receiver"),
	
	/** The rc 302. */
	RC_302("302", "Unable to locate record on file"),
	
	/** The rc 306. */
	RC_306("306", "Not successful"),
	
	/** The rc 307. */
	RC_307("307", "Format error"),
	
	/** The rc 400. */
	RC_400("400", "Accepted"),
	
	/** The rc 480. */
	RC_480("480", "Original transaction not found"),
	
	/** The rc 481. */
	RC_481("481", "Original transaction was found but declined"),
	
	/** The rc 500. */
	RC_500("500", "Reconciled, in balance"),
	
	/** The rc 501. */
	RC_501("501", "Reconciled, out of balance"),
	
	/** The rc 690. */
	RC_690("690", "Unable to parse message"),
	
	/** The rc 800. */
	RC_800("800", "Accepted"),
	
	/** The rc 888. */
	RC_888("888", "Unknown error"),
	
	/** The rc 893. */
	RC_893("893", "Signature Error"),
	
	/** The rc 902. */
	RC_902("902", "Invalid transaction"),
	
	/** The rc 903. */
	RC_903("903", "Re-enter transaction"),
	
	/** The rc 904. */
	RC_904("904", "Format error"),
	
	/** The rc 906. */
	RC_906("906", "Cutover in process"),
	
	/** The rc 907. */
	RC_907("907", "Card issuer or switch inoperative"),
	
	/** The rc 908. */
	RC_908("908", "Transaction destination cannot be found for routing"),
	
	/** The rc 909. */
	RC_909("909", "System malfunction"),
	
	/** The rc 910. */
	RC_910("910", "Card issuer signed off"),
	
	/** The rc 911. */
	RC_911("911", "Card issuer timed out"),
	
	/** The rc 912. */
	RC_912("912", "Card issuer unavailable"),
	
	/** The rc 913. */
	RC_913("913", "Duplicate transmission"),
	
	/** The rc 914. */
	RC_914("914", "Not able to trace back to original transaction"),
	
	/** The rc 915. */
	RC_915("915", "Reconciliation cutover or checkpoint error"),
	
	/** The rc 916. */
	RC_916("916", "MAC incorrect (permissible in 1644)"),
	
	/** The rc 917. */
	RC_917("917", "MAC key sync"),
	
	/** The rc 918. */
	RC_918("918", "No communication keys available for use"),
	
	/** The rc 919. */
	RC_919("919", "Encryption key sync error"),
	
	/** The rc 920. */
	RC_920("920", "Security software/hardware error – try again"),
	
	/** The rc 921. */
	RC_921("921", "Security software/hardware error – no action"),
	
	/** The rc 922. */
	RC_922("922", "Message number out of sequence"),
	
	/** The rc 923. */
	RC_923("923", "Request in progress"),
	
	/** The rc 940. */
    RC_940("940","Unknown terminal"),
    
    /** The rc 942. */
    RC_942("942", "Invalid Reconciliation Date");
    ;

	/** The code. */
    private final String code;

    /** The description. */
    private final String description;
    
    /**
     * Instantiates a new response codes.
     *
     * @param code the code
     * @param description the description
     */
    ResponseCodes(String code, String description) {
    	this.code = code;
    	this.description = description;
	}
    
    /**
     * Gets the response description.
     *
     * @param code the code
     * @return the response description
     */
    public static String getResponseDescription(String code) {
    	String responseDescription = null;
        for (ResponseCodes v : ResponseCodes.values()) {
            if (String.valueOf(v.code).equalsIgnoreCase(code)) {
                return v.description;
            }
        }
        return responseDescription;
    }
}
