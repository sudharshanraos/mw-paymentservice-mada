package com.tarang.middleware.paymentservice.enums;

/**
 * The Enum Status.
 * 
 * @author sudharshan.s
 */
public enum Status {

    /** The active. */
    ACTIVE,

    /** The inactive. */
    INACTIVE;

    /**
     * Gets the constant.
     *
     * @param name
     *            the name
     * @return the constant
     */
    public static Status getConstant(String name) {
        for (Status v : Status.values()) {
            if (v.name().equalsIgnoreCase(name)) {
                return v;
            }
        }
        return null;
    }

}
