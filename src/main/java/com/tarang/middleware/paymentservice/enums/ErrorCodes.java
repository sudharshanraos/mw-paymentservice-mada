package com.tarang.middleware.paymentservice.enums;

import lombok.Getter;

/**
 * The Enum ErrorCodes.
 * 
 * @author sudharshan.s
 */
@Getter
public enum ErrorCodes {

    /** The success. */
    SUCCESS(200, "success");

    /** The code. */
    private final int code;

    /** The description. */
    private final String description;

    /**
     * Instantiates a new config server error codes.
     *
     * @param code
     *            the code
     * @param description
     *            the description
     */
    ErrorCodes(int code, String description) {
        this.code = code;
        this.description = description;
    }

}
