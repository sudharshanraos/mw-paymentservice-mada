package com.tarang.middleware.paymentservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * The Class MadaPaymentServiceApplication.
 * 
 * @author sudharshan.s
 */
@EnableScheduling
@SpringBootApplication
public class MadaPaymentServiceApplication {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(MadaPaymentServiceApplication.class);

    /**
     * The main method.
     *
     * @param args
     *            the arguments
     */
    public static void main(String[] args) {
        LOGGER.info("MadaPaymentServiceApplication Starting..");
        SpringApplication.run(MadaPaymentServiceApplication.class, args);
    }
    
}
