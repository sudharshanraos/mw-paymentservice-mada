package com.tarang.middleware.paymentservice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tarang.middleware.paymentservice.constants.ApiConstants;
import com.tarang.middleware.paymentservice.dto.GeneralResponse;
import com.tarang.middleware.paymentservice.dto.TransactionRequest;
import com.tarang.middleware.paymentservice.dto.TransactionResponse;
import com.tarang.middleware.paymentservice.enums.TransactionTypes;
import com.tarang.middleware.paymentservice.service.AuthenticationService;
import com.tarang.middleware.paymentservice.service.io.PaymentHostService;
import com.tarang.middleware.paymentservice.service.iso.ISOMessageFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import liquibase.pro.packaged.T;

/**
 * This controller provides payment services for mada.
 *
 * @author sudharshan.s
 */
@RestController
@RequestMapping(ApiConstants.PS_MADA_PARENT)
@Api(value = ApiConstants.PAYMENT_SERVICE_MANAGEMENT, tags = {
        ApiConstants.PAYMENT_SERVICE_MANAGEMENT
})
public class PaymentServiceController {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentServiceController.class);

    /** The message factory. */
    @Autowired
    private ISOMessageFactory messageFactory;

    /** The payment host service. */
    @Autowired
    private PaymentHostService paymentHostService;
    
    /** The authentication service. */
    @Autowired
    private AuthenticationService authenticationService;

    /**
     * Auth.
     *
     * @param request
     *            the request
     * @return the transaction response
     */
    @PostMapping(value = ApiConstants.PS_MADA_AUTH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Process the auth transaction")
    @ResponseStatus(HttpStatus.OK)
    public TransactionResponse auth(@RequestBody TransactionRequest request) {
    	return authenticationService.authenticationTxnRequest(request);
    }

    /**
     * Sale.
     *
     * @param request
     *            the request
     * @return the transaction response
     */
    @PostMapping(value = ApiConstants.PS_MADA_SALE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Process the sale transaction")
    @ResponseStatus(HttpStatus.OK)
    public TransactionResponse sale(@RequestBody TransactionRequest request) {
        LOGGER.info("Process the sale transaction");
        try {
            paymentHostService.send(messageFactory.createISOMessage(request.getMessage(), TransactionTypes.FINANCIAL), request.getHost(),
                    request.getPort());
        } catch (Exception e) {
            LOGGER.error("ERROR: process the sale transaction - {}", e);
        }
        return new TransactionResponse();
    }

    /**
     * Refund.
     *
     * @param request
     *            the request
     * @return the transaction response
     */
    @PostMapping(value = ApiConstants.PS_MADA_REFEND, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Process the refund transaction")
    @ResponseStatus(HttpStatus.OK)
    public TransactionResponse refund(@RequestBody TransactionRequest request) {
        LOGGER.info("Process the refund transaction");
        try {
            paymentHostService.send(messageFactory.createISOMessage(request.getMessage(), TransactionTypes.REFUND), request.getHost(),
                    request.getPort());
        } catch (Exception e) {
            LOGGER.error("ERROR: process the refund transaction - {}", e);
        }
        return new TransactionResponse();
    }
}
