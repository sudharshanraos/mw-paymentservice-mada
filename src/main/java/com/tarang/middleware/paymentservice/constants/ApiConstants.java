package com.tarang.middleware.paymentservice.constants;

/**
 * The Interface ApiConstants.
 * 
 * @author sudharshan.s
 */
public final class ApiConstants {

    /** The Constant PARENT. */
    public static final String PARENT = "/";

    /** The Constant PAYMENT_SERVICE_MANAGEMENT. */
    public static final String PAYMENT_SERVICE_MANAGEMENT = "Payment Service Management";
    
    /** The Constant SYSTEM_CONFIG_MANAGEMENT. */
    public static final String SYSTEM_CONFIG_MANAGEMENT = "System Configuration Management";
    
    /** The Constant PS_MADA_PARENT. */
    public static final String PS_MADA_PARENT = "/mada";

    /** The Constant PS_MADA_AUTH. */
    public static final String PS_MADA_AUTH = "/auth";
    
    /** The Constant PS_MADA_SALE. */
    public static final String PS_MADA_SALE = "/sale";
    
    /** The Constant PS_MADA_REFEND. */
    public static final String PS_MADA_REFEND = "/refund";

    /** The Constant SYSTEM_PARENT. */
    public static final String SYSTEM_PARENT = "/system";

    /** The Constant SYSTEM_GET_ENCRYPT_DATA. */
    public static final String SYSTEM_GET_ENCRYPT_DATA = "/encrypt";

    /** The Constant SYSTEM_GET_DECRYPT_DATA. */
    public static final String SYSTEM_GET_DECRYPT_DATA = "/decrypt";

    /**
     * Instantiates a new api constants.
     */
    private ApiConstants() {
        // private constructor
    }

}
