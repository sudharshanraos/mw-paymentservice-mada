package com.tarang.middleware.paymentservice.constants;

public final class ConstantValue {

    // setting value process code, field 03
    public static String NOTXN = "000000";
    public static String PURCHASE = "000000";
    public static String PURCHASE_NAQD = "090000";
    public static String PURCHASE_REVERSAL = "000000";
    public static String CASH_ADVANCE = "010000";
    public static String REFUND = "200000";
    public static String PRE_AUTHORISATION = "900000";
    public static String PRE_AUTHORISATION_COMPLETE = "000000";
    public static String PURCHASE_ADVICE = "000000";
    public static String NO_OF_TXNS = "000000";
    public static String PURCHASE_ADVICE_FULL = "000000";
    public static String PURCHASE_ADVICE_PARTIAL = "000000";
    public static String PRE_AUTHORISATION_VOID = "220000";

    // for credit
    public static String NOTXN_CREDIT = "000000";
    public static String PURCHASE_CREDIT = "003000";
    public static String PURCHASE_CASHBACK_CREDIT = "093000";
    public static String PURCHASE_REVERSAL_CREDIT = "000000";
    public static String CASH_ADVANCE_CREDIT = "013000";
    public static String REFUND_CREDIT = "203000";
    public static String PRE_AUTHORISATION_CREDIT = "903000";
    public static String PRE_AUTHORISATION_COMPLET_CREDIT = "003000";
    public static String PURCHASE_ADVICE_CREDIT = "003000";
    public static String NO_OF_TXNS_CREDIT = "000000";

    // setting value for function code, field 24

    // Used in 1100 messages
    public static String AMOUNT_ACCURATE = "100";
    public static String AMOUNT_ESTIMATE = "101";
    // Used in 1120, 1121 messages
    public static String PREAUTH_INITIAL_COMPLETION = "182";
    public static String AUTHORIZATION_EXTENSION = "183";
    // Used in 1200 messages and Used in 1220, 1221 messages
    public static String FINANCIAL_TRANSACTION = "200";
    public static String APPROVED_AUTH_SAME_AMOUNT = "201";
    public static String APPROVED_AUTH_DIFF_AMOUNT = "202";
    // Used in 1304, 1305 messages file action
    public static String REPLACE_FIELDS_RECORD = "302";
    public static String REPLACE_ENTIRE_RECORD = "304";
    public static String REPLACE_FILE = "306";
    // Used in 1420, 1421 messages
    public static String TXN_NOT_COMPLETED = "400";
    // Used in 1524, 1525 messages
    public static String TERMINAL_RECONCILIATION = "570";
    public static String FORCE_RECONCILIATION = "571";
    // Used in 1644 messages
    public static String UNABLE_PARSE_MSG = "650";
    public static String MAC_ERROR = "691";
    // Used in 1804 messages
    public static String TERMINAL_REGISTRATION = "814";
    public static String ECHO_NETWORK_CODE = "301";

    // for a chip card are as follows
    public static String TERMINAL_PROCESSED = "1004";
    public static String ICC_PROCESSED = "1005";
    public static String UNDER_FLOOR_LIMIT = "1006"; // not used in mada
    public static String TER_RANDOM_SEL = "1503";
    public static String ONLINE_ICC = "1505";
    public static String ONLINE_CARD_ACCEPTOR = "1506";
    public static String ONLINE_TERMINAL = "1508";
    public static String ONLINE_CARD_ISSUER = "1509";
    public static String MERCHANT_SUSPICIOUS = "1511";
    public static String PRE_AUTHORIZATION_VOID = "1151";
    public static String PRE_AUTHORIZATION_EXTENSION = "1152";

    // Reason for a 1200 Financial Transaction Request for a chip card rather
    // than an 1100 Authorisation Message.
    public static String FALLBACK_ICCTOMSR = "1776";
    public static String CLESS_TXN = "1990";
    public static String CLESS_TXN_ADVICE = "1490";

    // 4000-4499 Reason for a reversal. The valid codes for an 1420 / 1421
    // Reversal Advice are as follows.
    public static String CUSTOMER_CANCELLATION = "4000";
    public static String UNSPECIFIED = "4001";
    public static String SUSPECTED_MALFUNC = "4002";
    public static String FORMAT_ERROR = "4003";
    public static String INCORRECT_AMOUNT = "4005";
    public static String RES_RECEIVED_LATE = "4006";
    public static String UNABLE_COMPLETE_TXN = "4007";
    public static String UNABLE_DELIVER_MSG = "4013";
    public static String INVALID_RESP = "4020";
    public static String TIMEOUT_WAITING_RESP = "4021";
    public static String MAC_FAILURE = "4351";

    // setting value for data element 48 ===> Digital signature
    // HAVE TO CONFIRM THIS VALUE FROM CLIENT
    public static String VENDOR_PUBLIC_KEY_INDEX = "01"; // 48.4
    public static String SAMA_KEY_INDEX = "00"; // 48.5

    public static String RANDOM_STRING_LENGTH_SEQUENCE_INDICATOR = "000010";
    public static String MADA_SIGNATURE_LENGTH = "000090"; // 48.8

    // SETTING VALUE MADA SIGNATURE DATA //48.9
    public static String MSD1 = "0001"; // 2BYTES

    public static String MSD2 = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"; // 106
    // BYTES
    public static String MSD3 = "00"; // 1 BYTES
    public static String MSD4 = "3021300906052B0E03021A050004"; // 14 BYTES OID
    public static String MSD5 = "14"; // 1BYTE
    public static String A0000000031010 = "VC";
    public static String A0000000032010 = "VC";
    // master
    public static String A0000000041010 = "MC";
    // maestro
    public static String A0000000043060 = "DM";
    // mada
    public static String A0000002281010 = "P1";
    public static String A0000002282010 = "P1";
    // union pay
    public static String A000000333010101 = "UP";
    public static String A000000333010102 = "UP";

    public static String A00000002501 = "AX";

    public static String A000000GN = "GN";

    public static String REFER_TO_CARD_ISSUER_VALUE = "107";
    public static String REFER_TO_000 = "000";

    // check online offline
    public static String APP_CRYPTOGRAM_ARQC = "80"; // online
    public static String APP_CRYPTOGRAM_TC = "40"; // offline
    public static String APP_CRYPTOGRAM_AAC = "00"; // decline

    public static String ADMIN_NOTIFICATION_RESPONSE_CODE = "690";

    public static String KEYED = "KEYED";
    public static String SWIPED = "SWIPED";
    public static String DIPPED = "DIPPED";
    public static String CONTACTLESS = "CONTACTLESS";

    public static String underline = "X_____________________";
    public static String doted_underscore = "_ _ _ _ _ _ _ _ _ _ _ _ _ _ ";
    public static String doted_line = "- - - - - - -  - - - - - - - ";

    public static String SAF_APPROVED = "087";
    public static String SAF_APPROVED_UNABLE = "089";
    public static String SAF_REJECTED = "190";
    public static String SAF_DECLINED = "188";
    public static String SAF_REFUND = "000";

    public static class Pin {
        public static final int MKSK_DES_INDEX_MK = 101;
        public static final int MKSK_DES_INDEX_WK_PIN = 102;
        public static final int MKSK_DES_INDEX_WK_TRACK = 103;
        public static final int MKSK_DES_INDEX_WK_MAC = 104;
        public static final int DUKPT_DES_INDEX = 1;
    }
}
