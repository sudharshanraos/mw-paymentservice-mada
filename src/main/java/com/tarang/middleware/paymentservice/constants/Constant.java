package com.tarang.middleware.paymentservice.constants;

public final class Constant {

    public static boolean HITTING_LIVE_SERVER = true;
    public static boolean VERSION_6_0_5 = false;
    // true - for mada Live server False - mada test server
    public static boolean PRODUCTION_KEYS = true;
    public static String[] apnName = {
            "MOBILY", "STC", "SKY_BAND", "Zain"
    };
    public static String[] apnList = {
            "POS-M2M", "POS.M2M", "IBS", "Geideam2m"
    };
    public static String[] mcc = {
            "420", "420", "420", "420"
    };
    public static String[] mnc = {
            "03", "01", "05", "04"
    };

    public static String AUTH = "AUTH";
    public static String AUTHORIZATION_ADVICE = "AUTHORIZATION_ADVICE";
    public static String AUTH_ADVISE = "AUTH_ADVISE";
    public static String AUTH_ADVISE_REPEAT = "AUTH_ADVISE_REPEAT";
    public static String FINANCIAL = "FINANCIAL";
    public static String FINANCIAL_ADVISE = "FINANCIAL_ADVISE";
    public static String FINANCIAL_ADVISE_REPEAT = "FINANCIAL_ADVISE_REPEAT";
    public static String FILEACTION = "FILEACTION";
    public static String FILEACTION_REPEAT = "FILEACTION_REPEAT";
    public static String REVERSAL = "REVERSAL";
    public static String REVERSAL_REPEAT = "REVERSAL_REPEAT";
    public static String RECONCILIATION_REPEAT = "RECONCILIATION_REPEAT";
    public static String ADMIN_NOTIFICATION = "ADMIN_NOTIFICATION";
    public static String NETWORK_MNGMT = "NETWORK_MNGMT";
    public static String OFFLINE_PURCHASE = "OFFLINE_PURCHASE";

    // for debit
    public static String NOTXN_DEBIT = "NOTXN_DEBIT";
    public static String PURCHASE_DEBIT = "PURCHASE_DEBIT";
    public static String PURCHASE_CASHBACK_DEBIT = "PURCHASE_CASHBACK_DEBIT";
    public static String PURCHASE_REVERSAL_DEBIT = "PURCHASE_REVERSAL_DEBIT";
    public static String CASH_ADVANCE_DEBIT = "CASH_ADVANCE_DEBIT";
    public static String REFUND_DEBIT = "REFUND_DEBIT";
    public static String PRE_AUTHORISATION_DEBIT = "PRE_AUTHORISATION_DEBIT";
    public static String PURCHASE_ADVICE_DEBIT = "PURCHASE_ADVICE_DEBIT";
    public static String PRE_AUTHORISATION_COMPLET_DEBIT = "PRE_AUTHORISATION_COMPLET_DEBIT";
    public static String RECONCILIATION_TXN_DEBIT = "RECONCILIATION_TXN_DEBIT";
    public static String NETWORK_MNGMT_TXN_DEBIT = "NETWORK_MNGMT_TXN_DEBIT";
    public static String FILEACTION_TXN_DEBIT = "FILEACTION_TXN_DEBIT";
    public static String ADMIN_NOTIFICATION_TXN_DEBIT = "ADMIN_NOTIFICATION_TXN_DEBIT";
    public static String OFFLINE_DEBIT = "OFFLINE_DEBIT";
    public static String SETTLEMENT_DEBIT = "SETTLEMENT_DEBIT";
    public static String BATCH_UPLOAD_DEBIT = "BATCH_UPLOAD_DEBIT";
    public static String SETTLEMENT_CLOSURE_DEBIT = "SETTLEMENT_CLOSURE_DEBIT";
    public static String SUMMARY_RPT_DEBIT = "SUMMARY_RPT_DEBIT";
    public static String REPRINT_LAST_TXN_DEBIT = "REPRINT_LAST_TXN_DEBIT";
    public static String SNAPSHOT_BALANCE_DEBIT = "SNAPSHOT_BALANCE_DEBIT";
    public static String RUNNING_BALANCE_DEBIT = "RUNNING_BALANCE_DEBIT";
    public static String FULLDOWNLOAD_DEBIT = "FULLDOWNLOAD_DEBIT";
    public static String PARTIALDOWNLOAD_DEBIT = "PARTIALDOWNLOAD_DEBIT";
    public static String NO_OF_TXNS_DEBIT = "NO_OF_TXNS_DEBIT";

    // for credit
    public static String NOTXN_CREDIT = "NOTXN_CREDIT";
    public static String PURCHASE_CREDIT = "PURCHASE_CREDIT";
    public static String PURCHASE_CASHBACK_CREDIT = "PURCHASE_CASHBACK_CREDIT";
    public static String PURCHASE_REVERSAL_CREDIT = "PURCHASE_REVERSAL_CREDIT";
    public static String CASH_ADVANCE_CREDIT = "CASH_ADVANCE_CREDIT";
    public static String REFUND_CREDIT = "REFUND_CREDIT";
    public static String PRE_AUTHORISATION_CREDIT = "PRE_AUTHORISATION_CREDIT";
    public static String PURCHASE_ADVICE_CREDIT = "PURCHASE_ADVICE_CREDIT";
    public static String PRE_AUTHORISATION_COMPLET_CREDIT = "PRE_AUTHORISATION_COMPLET_CREDIT";
    public static String RECONCILIATION_TXN_CREDIT = "RECONCILIATION_TXN_CREDIT";
    public static String NETWORK_MNGMT_TXN_CREDIT = "NETWORK_MNGMT_TXN_CREDIT";
    public static String FILEACTION_TXN_CREDIT = "FILEACTION_TXN_CREDIT";
    public static String ADMIN_NOTIFICATION_TXN_CREDIT = "ADMIN_NOTIFICATION_TXN_CREDIT";
    public static String OFFLINE_CREDIT = "OFFLINE_CREDIT";
    public static String SETTLEMENT_CREDIT = "SETTLEMENT_CREDIT";
    public static String BATCH_UPLOAD_CREDIT = "BATCH_UPLOAD_CREDIT";
    public static String SETTLEMENT_CLOSURE_CREDIT = "SETTLEMENT_CLOSURE_CREDIT";
    public static String SUMMARY_RPT_CREDIT = "SUMMARY_RPT_CREDIT";
    public static String REPRINT_LAST_TXN_CREDIT = "REPRINT_LAST_TXN_CREDIT";
    public static String SNAPSHOT_BALANCE_CREDIT = "SNAPSHOT_BALANCE_CREDIT";
    public static String RUNNING_BALANCE_CREDIT = "RUNNING_BALANCE_CREDIT";
    public static String FULLDOWNLOAD_CREDIT = "FULLDOWNLOAD_CREDIT";
    public static String PARTIALDOWNLOAD_CREDIT = "PARTIALDOWNLOAD_CREDIT";
    public static String NO_OF_TXNS_CREDIT = "NO_OF_TXNS_CREDIT";

    // setting value for function code
    public static String AMOUNT_ACCURATE = "AMOUNT_ACCURATE";
    public static String AMOUNT_ESTIMATE = "AMOUNT_ESTIMATE";
    public static String PREAUTH_INITIAL_COMPLETION = "PREAUTH_INITIAL_COMPLETION";
    public static String AUTHORIZATION_EXTENSION = "AUTHORIZATION_EXTENSION";
    public static String ORIGIN_FINANCIAL_REQ_ADV = "ORIGIN_FINANCIAL_REQ_ADV";
    public static String APPROVED_AUTH_SAME_AMOUNT = "APPROVED_AUTH_SAME_AMOUNT";
    public static String APPROVED_AUTH_DIFF_AMOUNT = "APPROVED_AUTH_DIFF_AMOUNT";
    public static String REPLACE_FIELDS_RECORD = "REPLACE_FIELDS_RECORD";
    public static String REPLACE_ENTIRE_RECORD = "REPLACE_ENTIRE_RECORD";
    public static String REPLACE_FILE = "REPLACE_FILE";
    public static String TXN_NOT_COMPLETED = "TXN_NOT_COMPLETED";
    public static String TERMINAL_RECONCILIATION = "TERMINAL_RECONCILIATION";
    public static String FORCE_RECONCILIATION = "FORCE_RECONCILIATION";
    public static String UNABLE_PARSE_MSG = "UNABLE_PARSE_MSG";
    public static String MAC_ERROR = "MAC_ERROR";
    public static String TERMINAL_REGISTRATION = "TERMINAL_REGISTRATION";

    // setting value for function code
    public static String TERMINAL_PROCESSED = "TERMINAL_PROCESSED";
    public static String ICC_PROCESSED = "ICC_PROCESSED";
    public static String UNDER_FLOOR_LIMIT = "UNDER_FLOOR_LIMIT";
    public static String ICC_RANDOM_SEL = "ICC_RANDOM_SEL";
    public static String TER_RANDOM_SEL = "TER_RANDOM_SEL";
    public static String ONLINE_ICC = "ONLINE_ICC";
    public static String ONLINE_CARD_ACCEPTOR = "ONLINE_CARD_ACCEPTOR";
    public static String ONLINE_TERMINAL = "ONLINE_TERMINAL";
    public static String ONLINE_CARD_ISSUER = "ONLINE_CARD_ISSUER";
    public static String MERCHANT_SUSPICIOUS = "MERCHANT_SUSPICIOUS";
    public static String FALLBACK_ICCTOMSR = "FALLBACK_ICCTOMSR";
    public static String CLESS_TXN = "CLESS_TXN";
    public static String CLESS_TXN_ADVICE = "CLESS_TXN_ADVICE";
    public static String CUSTOMER_CANCELLATION = "CUSTOMER_CANCELLATION";
    public static String UNSPECIFIED = "UNSPECIFIED";
    public static String SUSPECTED_MALFUNC = "SUSPECTED_MALFUNC";
    public static String FORMAT_ERROR = "FORMAT_ERROR";
    public static String INCORRECT_AMOUNT = "INCORRECT_AMOUNT";
    public static String RES_RECEIVED_LATE = "RES_RECEIVED_LATE";
    public static String UNABLE_COMPLETE_TXN = "UNABLE_COMPLETE_TXN";
    public static String UNABLE_DELIVER_MSG = "UNABLE_DELIVER_MSG";
    public static String INVALID_RESP = "INVALID_RESP";
    public static String TIMEOUT_WAITING_RESP = "TIMEOUT_WAITING_RESP";
    public static String MAC_FAILURE = "MAC_FAILURE";

    // Transaction Type's
    public static String PURCHASE = "PURCHASE";
    public static String NAQD_AMOUNT = "NAQD_AMOUNT";
    public static String PURCHASE_REVERSAL = "PURCHASE_REVERSAL";
    public static String REFUND = "REFUND";
    public static String CASH_ADVANCE = "CASH_ADVANCE";
    public static String PRE_AUTHORISATION = "PRE_AUTHORISATION";
    public static String PRE_AUTHORISATION_VOID = "PRE_AUTHORISATION_VOID";
    public static String PRE_AUTHORISATION_VOID_FULL = "PRE_AUTHORISATION_VOID_FULL";
    public static String PRE_AUTHORISATION_VOID_PARTIAL = "PRE_AUTHORISATION_VOID_PARTIAL";
    public static String PRE_AUTHORISATION_EXTENSION = "PRE_AUTHORISATION_EXTENSTION";
    public static String PURCHASE_ADVICE = "PURCHASE_ADVICE";
    public static String PURCHASE_ADVICE_FULL = "PURCHASE_ADVICE_FULL";
    public static String PURCHASE_ADVICE_PARTIAL = "PURCHASE_ADVICE_PARTIAL";
    public static String PURCHASE_ADVICE_MANUAL = "PURCHASE_ADVICE_MANUAL";
    public static String FINANCIAL_TRANSACTION = "FINANCIAL_TRANSACTION";
    public static String FALLBACK = "FALLBACK";
    public static String PRE_AUTHORISATION_COMPLETE = "PRE_AUTHORISATION_COMPLETE";
    public static String NO_OF_TXNS = "NO_OF_TXNS";
    public static String PURCHASE_WITH_NAQD = "PURCHASE_WITH_NAQD";
    public static String PURCHASE_WITH_NAQD_AR = "PURCHASE_WITH_NAQD_AR";
    public static String PURCHASE_NAQD = "PURCHASE_NAQD";
    public static String FINANCIAL_AUTHORIZATION = "FINANCIAL_AUTHORIZATION";

    // Transaction Mode's
    public static String MAGNETIC_STRIP = "MAGNETIC_STRIP";
    public static String EMV_CHIP = "EMV_CHIP";
    public static String CONTACT_LESS = "CONTACT_LESS";
    public static String MANUALLYENTER = "MANUALLYENTER";

    public static String TXT_PURCHASE = "Purchase";
    public static String TXT_PURCHASE_CASHBACK = "Purchase Cashback";
    public static String TXT_PURCHASE_NAQD = "Purchase with NAQD";
    public static String TXT_PURCHASE_REVERSAL = "Reversal";
    public static String TXT_REFUND = "Refund";
    public static String TXT_CASH_ADVANCE = "Cash Advance";
    public static String TXT_PRE_AUTHORISATION = "Pre Authorisation";
    public static String TXT_PRE_AUTHORISATION_VOID = "Preauth Void";
    public static String TXT_PRE_AUTHORISATION_EXTENSION = "Preauth Extension";
    public static String TXT_PURCHASE_ADVICE = "Purchase Advice";

    // card schemes
    public static String CARD_SCHEME_MADA = "MADA";
    public static String CARD_SCHEME_GCCNET = "GCCNET";
    public static String CARD_SCHEME_MASTER_CARD = "MASTERCARD";
    public static String CARD_SCHEME_MAESTRO = "MAESTRO";
    public static String CARD_SCHEME_VISA = "VISA";
    public static String CARD_SCHEME_AMERICAN_EXPRESS = "AMERICAN EXPRESS";
    public static String CARD_SCHEME_UNION_PAY = "UNION PAY";

    // visa
    public static String A0000000031010 = "A0000000031010";
    public static String A0000000032010 = "A0000000032010";
    // master
    public static String A0000000041010 = "A0000000041010";
    // maestro
    public static String A0000000043060 = "A0000000043060";
    // mada
    public static String A0000002281010 = "A0000002281010";
    public static String A0000002282010 = "A0000002282010";
    // union pay
    public static String A000000333010101 = "A000000333010101";
    public static String A000000333010102 = "A000000333010102";

    // American Express
    public static String A00000002501 = "A00000002501";

    // success response
    public static String SUCCESS_RESPONSE_000 = "000";
    public static String SUCCESS_RESPONSE_001 = "001";
    public static String SUCCESS_RESPONSE_003 = "003";
    public static String SUCCESS_RESPONSE_007 = "007";
    public static String SUCCESS_RESPONSE_800 = "800";
    public static String REVERSAL_RESPONSE_400 = "400";

    // check online offline
    public static String APP_CRYPTOGRAM_ARQC = "ONLINE"; // online
    public static String APP_CRYPTOGRAM_TC = "OFFLINE"; // offline
    public static String APP_CRYPTOGRAM_AAC = "DECLINE"; // decline

    public static String CVM_ONLINE_PIN_VALUE = "420000";
    public static String CVM_NO = "1F0000";
    public static String CVM_FAIL = "1F0001";
    public static String CVM_SIGNATURE = "1E0000";

}
