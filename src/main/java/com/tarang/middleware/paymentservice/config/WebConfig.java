package com.tarang.middleware.paymentservice.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * The Class WebConfig.
 * 
 * @author sudharshan.s
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    /**
     * Adds the cors mappings.
     *
     * @param registry
     *            the registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedMethods("*").allowedOrigins("Access-Control-Allow-Origin", "*");
    }

}
