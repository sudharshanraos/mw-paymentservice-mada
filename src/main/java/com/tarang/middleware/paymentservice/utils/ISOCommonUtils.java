/** 
 * COPYRIGHT: Comviva Technologies Pvt. Ltd.
 * This software is the sole property of Comviva
 * and is protected by copyright law and international
 * treaty provisions. Unauthorized reproduction or
 * redistribution of this program, or any portion of
 * it may result in severe civil and criminal penalties
 * and will be prosecuted to the maximum extent possible
 * under the law. Comviva reserves all rights not
 * expressly granted. You may not reverse engineer, decompile,
 * or disassemble the software, except and only to the
 * extent that such activity is expressly permitted
 * by applicable law notwithstanding this limitation.
 * THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 * YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY
 * AND THE USE OF THIS SOFTWARE. Comviva SHALL NOT BE LIABLE FOR
 * ANY DAMAGES WHATSOEVER ARISING OUT OF THE USE OF OR INABILITY TO
 * USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
package com.tarang.middleware.paymentservice.utils;

import java.math.BigInteger;
import java.util.Locale;

import org.jpos.iso.ISOUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This final class used for iso common utilities methods.
 * 
 * @author sudharshans
 */
public final class ISOCommonUtils {
    
    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ISOCommonUtils.class);


    /**
     * Instantiates a new iso common utils.
     */
    private ISOCommonUtils() {
    }

    /**
     * Adds the zeros if required.
     *
     * @param str
     *            the str
     * @return the string
     */
    public static String addZerosIfRequired(String str) {
        int length = ISOUtil.hex2byte(str).length * 2;
        for (int i = 0; i < 8; i++) {
            if (length % 8 != 0) {
                str = str + "00";
                length = ISOUtil.hex2byte(str).length;
            } else {
                return str;
            }
        }
        return "";
    }

    /**
     * Adds the zeros.
     *
     * @param amtTransaction4
     *            the amt transaction 4
     * @param i
     *            the i
     * @return the string
     */
    public static String addZeros(String amtTransaction4, int i) {
        long amount = Long.parseLong(amtTransaction4);
        String format = "%0" + i + "d";
        LOGGER.debug("amount --" + String.format(Locale.US, format, amount));
        return String.format(Locale.US, format, amount);
    }

    /**
     * Gets the hexa string.
     *
     * @param str
     *            the str
     * @return the hexa string
     */
    public static String getHexaString(String str) {
        return String.format(Locale.US, "%x", new BigInteger(1, str.getBytes()));
    }

    /**
     * Gets the hexa byte.
     *
     * @param str
     *            the str
     * @return the hexa byte
     */
    public static byte[] getHexaByte(String str) {
        return ISOUtil.hex2byte(String.format(Locale.US, "%x", new BigInteger(1, str.getBytes())));
    }

    /**
     * Gets the track 2 data validate.
     *
     * @param track2
     *            the track 2
     * @return the track 2 data validate
     */
    public String getTrack2DataValidate(String track2) {
        if (track2 == null || track2.trim().length() == 0)
            return "";
        if (track2.endsWith("F")) {
            LOGGER.debug("emvTransInfo.getTrack_2_eqv_data() -" + track2);
            track2 = track2.substring(0, track2.length() - 1);
        }
        LOGGER.debug("emvTrsanInfo.getTrack_2_eqv_data() -" + track2);
        if (37 <= track2.trim().length())
            return (track2.substring(0, 37));
        else
            return track2;
    }

    /**
     * Gets the string from hex.
     *
     * @param hex
     *            the hex
     * @return the string from hex
     */
    public static String getStringFromHex(String hex) {
        if (hex != null && hex.length() != 0) {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < hex.length(); i += 2) {
                str.append((char) Integer.parseInt(hex.substring(i, i + 2), 16));
            }
            return str.toString();
        } else
            return "";
    }
}
