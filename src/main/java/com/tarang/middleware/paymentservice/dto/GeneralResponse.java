package com.tarang.middleware.paymentservice.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This class contains the general response to be returned across API's in this
 * service.
 *
 * @author sudharshan.s
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GeneralResponse<T> implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The message. */
    private String message;

    /** The code. */
    private int code;

    /** The success. */
    @JsonProperty("isSuccess")
    private boolean success;

    /** The data. */
    private T data;

}
