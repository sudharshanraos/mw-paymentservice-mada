package com.tarang.middleware.paymentservice.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * The Class ISOBaseMessage.
 * 
 * @author sudharshan.s
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class ISOBaseMessage implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The mti 0. */
    private String mti0;

    /** The primary acc no 2. */
    private String primaryAccNo2;

    /** The processing code 3. */
    private String processingCode3;

    /** The amt transaction 4. */
    private String amtTransaction4;

    /** The amt sattlement 5. */
    private String amtSattlement5;

    /** The amt cardholderbilling 6. */
    private String amtCardholderbilling6;

    /** The transmission date time 7. */
    private String transmissionDateTime7;

    /** The amt cardholderbillingfee 8. */
    private String amtCardholderbillingfee8;

    /** The conversation rate sattlement 9. */
    private String conversationRateSattlement9;

    /** The conversation rate cardholderbilling 10. */
    private String conversationRateCardholderbilling10;

    /** The system trace auditnumber 11. */
    private String systemTraceAuditnumber11;

    /** The time local transaction 12. */
    private String timeLocalTransaction12;

    /** The date local transaction 13. */
    private String dateLocalTransaction13;

    /** The date expiration 14. */
    private String dateExpiration14;

    /** The date sattlement 15. */
    private String dateSattlement15;

    /** The date conversion 16. */
    private String dateConversion16;

    /** The date capture 17. */
    private String dateCapture17;

    /** The merchant type 18. */
    private String merchantType18;

    /** The country code acquiringinstitution 19. */
    private String countryCodeAcquiringinstitution19;

    /** The country code primaryaccountno 20. */
    private String countryCodePrimaryaccountno20;

    /** The country code forwardinginstitution 21. */
    private String countryCodeForwardinginstitution21;

    /** The pos entrymode 22. */
    private String posEntrymode22;

    /** The card sequence number 23. */
    private String cardSequenceNumber23;

    /** The functioncode 24. */
    private String functioncode24;

    /** The message reason code 25. */
    private String messageReasonCode25;

    /** The card acceptor business code 26. */
    private String cardAcceptorBusinessCode26;

    /** The autid res length 27. */
    private String autidResLength27;

    /** The reconsilation date 28. */
    private String reconsilationDate28;

    /** The amt sattlement fee 29. */
    private String amtSattlementFee29;

    /** The amt tran processing fee 30. */
    private String amtTranProcessingFee30;

    /** The amt sattle processing fee 31. */
    private String amtSattleProcessingFee31;

    /** The accuring insitute id code 32. */
    private String accuringInsituteIdCode32;

    /** The forward insitute id code 33. */
    private String forwardInsituteIdCode33;

    /** The primary account number extended 34. */
    private String primaryAccountNumberExtended34;

    /** The track 2 data 35. */
    private String track2Data35;

    /** The track 3 data 36. */
    private String track3Data36;

    /** The retri ref no 37. */
    private String retriRefNo37;

    /** The auth id res code 38. */
    private String authIdResCode38;

    /** The response code 39. */
    private String responseCode39;

    /** The service restric code 40. */
    private String serviceRestricCode40;

    /** The card acceptor tem id 41. */
    private String cardAcceptorTemId41;

    /** The card acceptor id code 42. */
    private String cardAcceptorIdCode42;

    /** The card acceptor name location 43. */
    private String cardAcceptorNameLocation43;

    /** The additional res data 44. */
    private String additionalResData44;

    /** The track one data 45. */
    private String trackOneData45;

    /** The amounts fees 46. */
    private String amountsFees46;

    /** The additional data national 47. */
    private String additionalDataNational47;

    /** The additional data private 48. */
    private String additionalDataPrivate48;

    /** The curr code transaction 49. */
    private String currCodeTransaction49;

    /** The curr code statle ment 50. */
    private String currCodeStatleMent50;

    /** The currency code cardholderbilling 51. */
    private String currencyCodeCardholderbilling51;

    /** The pin data 52. */
    private String pinData52;

    /** The sec related cont info 53. */
    private String secRelatedContInfo53;

    /** The addl amt 54. */
    private String addlAmt54;

    /** The icc card system related data 55. */
    private String iccCardSystemRelatedData55;

    /** The msg reason code 56. */
    private String msgReasonCode56;

    /** The auth life code 57. */
    private String authLifeCode57;

    /** The auth agent id code 58. */
    private String authAgentIdCode58;

    /** The echo data 59. */
    private String echoData59;

    /** The reserved data 60. */
    private String reservedData60;

    /** The reserved data 61. */
    private String reservedData61;

    /** The reserved data 62. */
    private String reservedData62;

    /** The reserved data 63. */
    private String reservedData63;

    /** The message authentication code field 64. */
    private String messageAuthenticationCodeField64;

    /** The reserved data 65. */
    private String reservedData65;

    /** The statle ment code 66. */
    private String statleMentCode66;

    /** The ext payment code 67. */
    private String extPaymentCode67;

    /** The country code receiving institution 68. */
    private String countryCodeReceivingInstitution68;

    /** The country code settlement institution 69. */
    private String countryCodeSettlementInstitution69;

    /** The net mgn info code 70. */
    private String netMgnInfoCode70;

    /** The message number 71. */
    private String messageNumber71;

    /** The data record 72. */
    private String dataRecord72;

    /** The date action 73. */
    private String dateAction73;

    /** The credit no 74. */
    private String creditNo74;

    /** The credit rev no 75. */
    private String creditRevNo75;

    /** The debit no 76. */
    private String debitNo76;

    /** The debit rev no 77. */
    private String debitRevNo77;

    /** The transper no 78. */
    private String transperNo78;

    /** The transper rev no 79. */
    private String transperRevNo79;

    /** The inquiries no 80. */
    private String inquiriesNo80;

    /** The auth no 81. */
    private String authNo81;

    /** The credit process fee amt 82. */
    private String creditProcessFeeAmt82;

    /** The credit trans fee amt 83. */
    private String creditTransFeeAmt83;

    /** The debit process fee amt 84. */
    private String debitProcessFeeAmt84;

    /** The debit trans fee amt 85. */
    private String debitTransFeeAmt85;

    /** The credit amt 86. */
    private String creditAmt86;

    /** The credit rev amt 87. */
    private String creditRevAmt87;

    /** The debit amt 88. */
    private String debitAmt88;

    /** The debit rev amt 89. */
    private String debitRevAmt89;

    /** The org data element 90. */
    private String orgDataElement90;

    /** The file update code 91. */
    private String fileUpdateCode91;

    /** The country code transaction orig 92. */
    private String countryCodeTransactionOrig92;

    /** The transaction dest 93. */
    private String transactionDest93;

    /** The transaction orig 94. */
    private String transactionOrig94;

    /** The replace amt 95. */
    private String replaceAmt95;

    /** The key management data 96. */
    private String keyManagementData96;

    /** The amt net sattle 97. */
    private String amtNetSattle97;

    /** The payee 98. */
    private String payee98;

    /** The settlement institution id code 99. */
    private String settlementInstitutionIdCode99;

    /** The receive insitute id code 100. */
    private String receiveInsituteIdCode100;

    /** The file name 101. */
    private String fileName101;

    /** The acc ident one 102. */
    private String accIdentOne102;

    /** The acc ident two 103. */
    private String accIdentTwo103;

    /** The reserved data 104. */
    private String reservedData104;

    /** The credits chargeback amount 105. */
    private String creditsChargebackAmount105;

    /** The debits chargeback amount 106. */
    private String debitsChargebackAmount106;

    /** The credits chargeback number 107. */
    private String creditsChargebackNumber107;

    /** The debits chargeback number 108. */
    private String debitsChargebackNumber108;

    /** The credits fee amounts 109. */
    private String creditsFeeAmounts109;

    /** The debits fe amounts 110. */
    private String debitsFeAmounts110;

    /** The reserved data 111. */
    private String reservedData111;

    /** The reserved data 112. */
    private String reservedData112;

    /** The reserved data 113. */
    private String reservedData113;

    /** The reserved data 114. */
    private String reservedData114;

    /** The reserved data 115. */
    private String reservedData115;

    /** The reserved data 116. */
    private String reservedData116;

    /** The reserved data 117. */
    private String reservedData117;

    /** The payment no 118. */
    private String paymentNo118;

    /** The payment rev no 119. */
    private String paymentRevNo119;

    /** The reserved data 120. */
    private String reservedData120;

    /** The reserved data 121. */
    private String reservedData121;

    /** The reserved data 122. */
    private String reservedData122;

    /** The pos data code 123. */
    private String posDataCode123;

    /** The reserved data 124. */
    private String reservedData124;

    /** The net mgn info 125. */
    private String netMgnInfo125;

    /** The reserved data 126. */
    private String reservedData126;

    /** The switch key 127 2. */
    private String switchKey127_2;

    /** The route info 127 3. */
    private String routeInfo127_3;

    /** The pos data 127 4. */
    private String posData127_4;

    /** The ser station data 127 5. */
    private String serStationData127_5;

    /** The auth profile 127 6. */
    private String authProfile127_6;

    /** The check data 127 7. */
    private String checkData127_7;

    /** The retention data 127 8. */
    private String retentionData127_8;

    /** The addl node data 127 9. */
    private String addlNodeData127_9;

    /** The cvv 2127 10. */
    private String cvv2127_10;

    /** The org key 127 11. */
    private String orgKey127_11;

    /** The term owner 127 12. */
    private String termOwner127_12;

    /** The pos geograpic data 127 13. */
    private String posGeograpicData127_13;

    /** The sponsor bank 127 14. */
    private String sponsorBank127_14;

    /** The address verific data 127 15. */
    private String addressVerificData127_15;

    /** The address verific result 127 16. */
    private String addressVerificResult127_16;

    /** The card holder info 127 17. */
    private String cardHolderInfo127_17;

    /** The validation data 127 18. */
    private String validationData127_18;

    /** The bank detail 127 19. */
    private String bankDetail127_19;

    /** The orig or authorizer date stattle ment 127 20. */
    private String origOrAuthorizerDateStattleMent127_20;

    /** The record identification 127 21. */
    private String recordIdentification127_21;

    /** The srtucre data 127 22. */
    private String srtucreData127_22;

    /** The payee name address 127 23. */
    private String payeeNameAddress127_23;

    /** The payer acc 127 24. */
    private String payerAcc127_24;

    /** The icc data 127 25. */
    private String iccData127_25;

    /** The amt auth data 127 25 2. */
    private String amtAuthData127_25_2;

    /** The amt other data 127 25 3. */
    private String amtOtherData127_25_3;

    /** The app identifier data 127 25 4. */
    private String appIdentifierData127_25_4;

    /** The app inter change prifile data 127 25 5. */
    private String appInterChangePrifileData127_25_5;

    /** The app trans counter data 127 25 6. */
    private String appTransCounterData127_25_6;

    /** The app usage data 127 25 7. */
    private String appUsageData127_25_7;

    /** The authorization response code 127 25 8. */
    private String authorizationResponseCode127_25_8;

    /** The card authentication reliability indicator 127 25 9. */
    private String cardAuthenticationReliabilityIndicator127_25_9;

    /** The card authentication results code 127 25 10. */
    private String cardAuthenticationResultsCode127_25_10;

    /** The chip condition code 127 25 11. */
    private String chipConditionCode127_25_11;

    /** The app cryptogram data 127 25 12. */
    private String appCryptogramData127_25_12;

    /** The app crypto gram info data 127 25 13. */
    private String appCryptoGramInfoData127_25_13;

    /** The cvm list 127 25 14. */
    private String cvmList127_25_14;

    /** The cvm result data 127 25 15. */
    private String cvmResultData127_25_15;

    /** The interface device serial number 127 25 16. */
    private String interfaceDeviceSerialNumber127_25_16;

    /** The issuer acc code data 127 25 17. */
    private String issuerAccCodeData127_25_17;

    /** The issue app data 127 25 18. */
    private String issueAppData127_25_18;

    /** The issuer script results 127 25 19. */
    private String issuerScriptResults127_25_19;

    /** The terminal application version number 127 25 20. */
    private String terminalApplicationVersionNumber127_25_20;

    /** The term capable data 127 25 21. */
    private String termCapableData127_25_21;

    /** The term coun code data 127 25 22. */
    private String termCounCodeData127_25_22;

    /** The term type data 127 25 23. */
    private String termTypeData127_25_23;

    /** The term ver res data 127 25 24. */
    private String termVerResData127_25_24;

    /** The transaction category code 127 25 25. */
    private String transactionCategoryCode127_25_25;

    /** The tran curr code data 127 25 26. */
    private String tranCurrCodeData127_25_26;

    /** The tran date data 127 25 27. */
    private String tranDateData127_25_27;

    /** The transaction sequence counter 127 25 28. */
    private String transactionSequenceCounter127_25_28;

    /** The tran type data 127 25 29. */
    private String tranTypeData127_25_29;

    /** The unpretictable data 127 25 30. */
    private String unpretictableData127_25_30;

    /** The issuer authentication data 127 25 31. */
    private String issuerAuthenticationData127_25_31;

    /** The issuer script template one 127 25 32. */
    private String issuerScriptTemplateOne127_25_32;

    /** The issuer script template two 127 25 33. */
    private String issuerScriptTemplateTwo127_25_33;

    /** The org node 127 26. */
    private String orgNode127_26;

    /** The card ver result 127 27. */
    private String cardVerResult127_27;

    /** The american exp card identifier 127 28. */
    private String americanExpCardIdentifier127_28;

    /** The secure data 127 29. */
    private String secureData127_29;

    /** The secure result 127 30. */
    private String secureResult127_30;

    /** The issuer net id 127 31. */
    private String issuerNetId127_31;

    /** The ucaf data 127 32. */
    private String ucafData127_32;

    /** The extented trans type 127 33. */
    private String extentedTransType127_33;

    /** The acc type qualifiers 127 34. */
    private String accTypeQualifiers127_34;

    /** The acquire net id 127 35. */
    private String acquireNetId127_35;

    /** The cus id 127 36. */
    private String cusId127_36;

    /** The ext res code 127 37. */
    private String extResCode127_37;

    /** The addl pos data code 127 38. */
    private String addlPosDataCode127_38;

    /** The org res code 127 39. */
    private String orgResCode127_39;

    /** The mac ext 128. */
    private String macExt128;

}
