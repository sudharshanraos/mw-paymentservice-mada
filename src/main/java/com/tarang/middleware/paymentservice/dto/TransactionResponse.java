package com.tarang.middleware.paymentservice.dto;

import java.io.Serializable;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * The Class TransactionResponse.
 * 
 * @author sudharshan.s
 */
@Getter
@Setter
@Data
public class TransactionResponse implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The message. */
    private boolean isSuccess;

    /** The code. */
    private String code;
    
    /** The code. */
    private String description;

    /** The message. */
    private ISOBaseMessage message;

}
