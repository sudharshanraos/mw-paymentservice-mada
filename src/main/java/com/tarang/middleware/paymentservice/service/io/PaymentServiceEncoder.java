package com.tarang.middleware.paymentservice.service.io;

import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.tarang.middleware.paymentservice.service.iso.ISOMessageFactory;
import com.tarang.middleware.paymentservice.utils.ByteConversionUtils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * The Class PaymentServiceEncoder.
 */
@Sharable
@Service
public class PaymentServiceEncoder extends MessageToByteEncoder<ISOMsg> {

    /** The Constant log. */
    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentServiceEncoder.class);

    /** The message factory. */
    @Autowired
    private ISOMessageFactory messageFactory;

    /** The hitting live server. */
    @Value("${paymentservice.hittingLiveServer}")
    private boolean hittingLiveServer;

    /** The tpdu header. */
    private String TPDU_HEADER = "6000370000";

    /**
     * Encode.
     *
     * @param ctx
     *            the ctx
     * @param msg
     *            the msg
     * @param out
     *            the out
     * @throws Exception
     *             the exception
     */
    @Override
    public void encode(ChannelHandlerContext ctx, ISOMsg isoMsg, ByteBuf out) throws Exception {
        try {
            byte[] isoBuffer = messageFactory.packISOMsg(isoMsg);
            byte[] finalIsoData;
            if (hittingLiveServer) {
                byte[] tpduHeader = ISOUtil.hex2byte(TPDU_HEADER);
                byte[] isoBufferTpduHeader = ISOUtil.concat(tpduHeader, isoBuffer);
                byte[] isoLength = ByteConversionUtils.intToByteArray(isoBufferTpduHeader.length);
                finalIsoData = ISOUtil.concat(isoLength, isoBufferTpduHeader);
                LOGGER.info("HEX >>> {}", ByteConversionUtils.byteArrayToHexString(finalIsoData, finalIsoData.length, false));
            } else {
                byte[] isoLength = ByteConversionUtils.intToByteArray(isoBuffer.length);
                finalIsoData = ISOUtil.concat(isoLength, isoBuffer);
                LOGGER.info("HEX >>> {}", ByteConversionUtils.byteArrayToHexString(finalIsoData, finalIsoData.length, false));
            }
            // TODO: Hardcoded
           // String data = "022760003700003131303037323330303743313238433238413035313634383933313938313636343330373438303030303030303030303030303030313131303732313133333835363130303132313230303732313136333835363731303330313531313334433030313130303135303633303035303635383838343933333438393331393831363634333037343844313931323232303135373138333031303032303533363136333835363138323235353838313233343536373832373131323031393135333420202030303652414A425031363832313318FFFF1814787480002C36303931393382027C009F02060000000001119F030600000000000050046D6164619F120A6D6164612044656269749F360203609F2608DC0A90183FFE3C8A9F2701809F34034403028407A00000022820109F101706010A03A4A0020F04000000000000000000007E2929349F1E0830303030303030319F350122950500C00060009F1A0206825F2A0206829A032007219F3704DA2EF8AA9C010057114893198166430748D1912220157183010F4F07A00000022820105A0848931981664307489F3303E0F8C8313136303131303232303330303030333030343030353030374e3233353234374530343331333437303930313030303030303030303031313030303030303030303132313532373232303030313331353237323530303031343032303331353135323732343135303630303035313630313031303130322411AD9EFFFFFFFF";
           // String sData = "021A6000370000313230303732333030374331323843323941303531363432383333313031303433303434383330303030303030303030303030303536353531323137313431323334313030323233313831323137313731323334373130333031373133333443303030323030313939303733393930363538383834373337343238333331303130343330343438333D31383132323231313236343834323230303030303137383138393039343230303132333435363738313231323132333438303031353034303035363620202030303652414A4250313638328ACF78FF29F7B843313318FFFF00362811000088363039313638820220009A031812179C01005A08428331010430448350046D6164619F02060000000056559F03060000000000009F120A6D6164612044656269749F3602014F9F100706011103A000009F1A0206829F3704D5F0E8649F6E04207000009F26088CED488E266F6E8A9F2701809F6C02B6009F660436E0C0005F2A0206828407A0000002282010950500000000009F1E0833333130303230399F34030200009F3303E0F8C89F350122313136303131303232303330303030333030343030353030374E343032363436573037393538353630393031303030303030303030303131303030303030303030313231353138353230303031333135313835343030303134303239303636313630313538313530363030303931363032303130313036B242A806FFFFFFFF";
           // finalIsoData = ISOUtil.hex2byte(data);
            out.writeBytes(finalIsoData);
        } catch (Exception e) {
            LOGGER.error("Unable to encode data : {}", e);
            throw new IllegalStateException("unable to encode data, " + e.getMessage());
        }
    }

}
