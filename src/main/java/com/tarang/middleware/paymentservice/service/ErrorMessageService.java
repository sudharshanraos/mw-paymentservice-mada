package com.tarang.middleware.paymentservice.service;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;

/**
 * The Class MessageServiceImpl.
 * 
 * @author sudharshan.s
 */
@Service
public class ErrorMessageService {

    /** The Constant LOGGER. */
    private static final Logger log = LoggerFactory.getLogger(ErrorMessageService.class);

    /** The Constant NO_SUCH_MESSAGE_EXCEPTION_OCCURRED. */
    private static final String NO_SUCH_MESSAGE_EXCEPTION_OCCURRED = "No Such Message Exception occurred";

    /** The message source. */
    @Autowired
    private MessageSource messageSource;

    /**
     * This method return the error message associated with an error code.
     *
     * @param errorCode
     *            : Error Code
     * @param objects
     *            the objects
     * @return Error message
     */
    public String getMessage(final String errorCode, Object[] objects) {
        try {
            return messageSource.getMessage(errorCode, objects, Locale.getDefault());
        } catch (NoSuchMessageException e) {
            log.debug(NO_SUCH_MESSAGE_EXCEPTION_OCCURRED, e.getMessage());
            log.trace(NO_SUCH_MESSAGE_EXCEPTION_OCCURRED, e);
            return errorCode;
        }
    }

}
