package com.tarang.middleware.paymentservice.service;

import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarang.middleware.paymentservice.dto.ISOBaseMessage;
import com.tarang.middleware.paymentservice.dto.TransactionRequest;
import com.tarang.middleware.paymentservice.dto.TransactionResponse;
import com.tarang.middleware.paymentservice.enums.ResponseCodes;
import com.tarang.middleware.paymentservice.enums.TransactionTypes;
import com.tarang.middleware.paymentservice.service.io.PaymentHostService;
import com.tarang.middleware.paymentservice.service.iso.ISOMessageFactory;

/**
 * The Class AuthenticationService.
 */
@Service
public class AuthenticationService {
	
	/** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationService.class);

    /** The payment host service. */
    @Autowired
    private PaymentHostService paymentHostService;
    
    /** The message factory. */
    @Autowired
    private ISOMessageFactory messageFactory;
    
    /** The transaction request. */
    private TransactionRequest transactionRequest;
    
    /** The transaction response. */
    private TransactionResponse transactionResponse;

	
	/**
	 * Authentication txn request.
	 *
	 * @param request the request
	 * @return the transaction response
	 */
	public TransactionResponse authenticationTxnRequest(TransactionRequest request) {
		LOGGER.info("Process the auth transaction");
		this.transactionRequest = request;
		this.transactionResponse = new TransactionResponse();
		
        try {
        	ISOMsg responseMsg = paymentHostService.send(messageFactory.createISOMessage(request.getMessage(), TransactionTypes.AUTH), request.getHost(),
                    request.getPort());
        	parseAndCreateResponse(responseMsg);
        } catch (Exception e) {
            LOGGER.error("ERROR: process the auth transaction - {}", e);
            transactionResponse.setSuccess(Boolean.FALSE);
        }
		
		return transactionResponse;
		
	}


	/**
	 * Parses the and create response.
	 *
	 * @param responseMsg the response msg
	 */
	private void parseAndCreateResponse(ISOMsg responseMsg) {
		ISOBaseMessage isoBaseMsg = transactionRequest.getMessage();
		isoBaseMsg.setRetriRefNo37(responseMsg.getString(37));
		isoBaseMsg.setAuthIdResCode38(responseMsg.getString(38));
		String responseCode = responseMsg.getString(39);
		isoBaseMsg.setResponseCode39(responseCode);
		transactionResponse.setCode(responseCode);
		transactionResponse.setDescription(ResponseCodes.getResponseDescription(responseCode));
		transactionResponse.setMessage(isoBaseMsg);
		transactionResponse.setSuccess(Boolean.TRUE);
		
	}
}
