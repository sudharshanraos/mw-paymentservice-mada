package com.tarang.middleware.paymentservice.service.iso;

import java.io.PrintStream;

import org.jpos.iso.ISOBinaryField;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tarang.middleware.paymentservice.constants.Constant;
import com.tarang.middleware.paymentservice.constants.ConstantValue;
import com.tarang.middleware.paymentservice.dto.ISOBaseMessage;
import com.tarang.middleware.paymentservice.enums.TransactionTypes;
import com.tarang.middleware.paymentservice.utils.ByteConversionUtils;
import com.tarang.middleware.paymentservice.utils.CommonUtils;

/**
 * The Class ISORequest.
 * 
 * @author sudharshan.s
 */
@Service
public class ISOMessageFactory {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ISOMessageFactory.class);

    /**
     * Creates a new ISOMessage object.
     *
     * @param reqObj
     *            the req obj
     * @param transactionType
     *            the transaction type
     * @return the ISO msg
     */
    public ISOMsg createISOMessage(ISOBaseMessage reqObj, TransactionTypes transactionType) {
        // Create the ISO message
        ISOMsg isoMsg = new ISOMsg();
        try {
            isoMsg.setMTI(transactionType.getReqMTI());
            if (!CommonUtils.isNullorEmpty(reqObj.getPrimaryAccNo2())) {
                isoMsg.set(new ISOField(2, reqObj.getPrimaryAccNo2()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getProcessingCode3())) {
                isoMsg.set(new ISOField(3, reqObj.getProcessingCode3()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getAmtTransaction4())) {
                isoMsg.set(new ISOField(4, addZeros(reqObj.getAmtTransaction4(), 12)));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getTransmissionDateTime7())) {
                isoMsg.set(new ISOField(7, reqObj.getTransmissionDateTime7()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getSystemTraceAuditnumber11())) {
                isoMsg.set(new ISOField(11, addZeros(reqObj.getSystemTraceAuditnumber11(), 6)));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getTimeLocalTransaction12())) {
                isoMsg.set(new ISOField(12, reqObj.getTimeLocalTransaction12()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getDateExpiration14())) {
                isoMsg.set(new ISOField(14, reqObj.getDateExpiration14()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getPosEntrymode22())) {
                isoMsg.set(new ISOField(22, reqObj.getPosEntrymode22()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getCardSequenceNumber23())) {
                isoMsg.set(new ISOField(23, reqObj.getCardSequenceNumber23()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getFunctioncode24())) {
                isoMsg.set(new ISOField(24, reqObj.getFunctioncode24()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getMessageReasonCode25())) {
                isoMsg.set(new ISOField(25, reqObj.getMessageReasonCode25()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getCardAcceptorBusinessCode26())) {
                isoMsg.set(new ISOField(26, reqObj.getCardAcceptorBusinessCode26()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getReconsilationDate28())) {
                isoMsg.set(new ISOField(28, reqObj.getReconsilationDate28()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getAmtTranProcessingFee30())) {
                isoMsg.set(new ISOField(30, reqObj.getAmtTranProcessingFee30()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getAccuringInsituteIdCode32())) {
                isoMsg.set(new ISOField(32, reqObj.getAccuringInsituteIdCode32()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getTrack2Data35())) {
                isoMsg.set(new ISOField(35, formatTrack2Data(reqObj.getTrack2Data35())));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getRetriRefNo37())) {
                isoMsg.set(new ISOField(37, reqObj.getRetriRefNo37()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getAuthIdResCode38())) {
                isoMsg.set(new ISOField(38, reqObj.getAuthIdResCode38()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getResponseCode39())) {
                isoMsg.set(new ISOField(39, reqObj.getResponseCode39()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getCardAcceptorTemId41())) {
                isoMsg.set(new ISOField(41, reqObj.getCardAcceptorTemId41()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getCardAcceptorIdCode42())) {
                isoMsg.set(new ISOField(42, reqObj.getCardAcceptorIdCode42()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getAdditionalResData44())) {
                isoMsg.set(new ISOField(44, reqObj.getAdditionalResData44()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getAdditionalDataNational47())) {
                isoMsg.set(new ISOField(47, reqObj.getAdditionalDataNational47()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getAdditionalDataPrivate48())) {
                isoMsg.set(new ISOBinaryField(48, ISOUtil.hex2byte(reqObj.getAdditionalDataPrivate48())));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getCurrCodeTransaction49())) {
                isoMsg.set(new ISOField(49, reqObj.getCurrCodeTransaction49()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getCurrCodeStatleMent50())) {
                isoMsg.set(new ISOField(50, reqObj.getCurrCodeStatleMent50()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getPinData52())) {
                isoMsg.set(new ISOBinaryField(52, ISOUtil.hex2byte(reqObj.getPinData52())));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getSecRelatedContInfo53())) {
                isoMsg.set(new ISOBinaryField(53, ISOUtil.hex2byte(reqObj.getSecRelatedContInfo53())));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getAddlAmt54())) {
                isoMsg.set(new ISOField(54, reqObj.getAddlAmt54()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getIccCardSystemRelatedData55())) {
                isoMsg.set(new ISOBinaryField(55, ISOUtil.hex2byte(reqObj.getIccCardSystemRelatedData55())));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getMsgReasonCode56())) {
                isoMsg.set(new ISOField(56, reqObj.getMsgReasonCode56()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getEchoData59())) {
                isoMsg.set(new ISOField(59, reqObj.getEchoData59()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getReservedData62())) {
                isoMsg.set(new ISOField(62, reqObj.getReservedData62()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getMessageAuthenticationCodeField64())) {
                isoMsg.set(new ISOBinaryField(64, ISOUtil.hex2byte(reqObj.getMessageAuthenticationCodeField64())));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getDataRecord72())) {
                isoMsg.set(new ISOField(72, reqObj.getDataRecord72()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getReservedData124())) {
                isoMsg.set(new ISOField(124, reqObj.getReservedData124()));
            }
            if (!CommonUtils.isNullorEmpty(reqObj.getMacExt128())) {
                isoMsg.set(new ISOBinaryField(128, ISOUtil.hex2byte(reqObj.getMacExt128())));
            }
        } catch (Exception e) {
            LOGGER.error("ERROR: create jpos iso message with json iso base message - {}", e);
        }
        return isoMsg;
    }

    /**
     * Pack ISO msg.
     *
     * @param isoMsg
     *            the iso msg
     * @return the byte[]
     */
    public byte[] packISOMsg(ISOMsg isoMsg) {
        byte[] isoRequest = new byte[1024];
        try {
            // Packing the ISO
            isoMsg.setPackager(new MadaPackager());
            isoRequest = isoMsg.pack();
            LOGGER.info("STRING >>> {} ", ByteConversionUtils.byteArrayToString(isoRequest, isoRequest.length));
            LOGGER.info("HEX >>> {}", ByteConversionUtils.byteArrayToHexString(isoRequest, isoRequest.length, true));
            isoMsg.dump(new PrintStream(System.err), "");
        } catch (Exception e) {
            LOGGER.error("ERROR: create iso request with ISO message - {}", e);
        }
        return isoRequest;
    }

    /**
     * This method unpacks the raw data and packages it into a ISOMessage format
     * as specified by the format of the packager that ViVO Pay determines. It
     * also validates the generated ISOMessage
     *
     * @param rawBytes
     *            the raw bytes
     * @return ISOMsg
     * @throws ISOException
     *             the ISO exception
     */
    public ISOMsg unpackISOFormat(byte[] rawBytes) throws ISOException {
        ISOMsg isoMessage = new ISOMsg();
        try {
            isoMessage.setPackager(new MadaPackager());
            LOGGER.info("STRING >>> {} ", ByteConversionUtils.byteArrayToString(rawBytes, rawBytes.length));
            LOGGER.info("HEX >>> {}", ByteConversionUtils.byteArrayToHexString(rawBytes, rawBytes.length, false));
            isoMessage.unpack(rawBytes);
            isoMessage.dump(new PrintStream(System.err), "");
        } catch (Exception e) {
            LOGGER.error("ERROR: unpacking the iso request - {}", e);
        }
        return isoMessage;
    }

    /**
     * Gets the message code.
     *
     * @param valueMessageCode
     *            the value message code
     * @return the message code
     */
    public String getMessageCode(String valueMessageCode) {
        if (valueMessageCode.equalsIgnoreCase(Constant.TERMINAL_PROCESSED)) {
            valueMessageCode = ConstantValue.TERMINAL_PROCESSED;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.ICC_PROCESSED)) {
            valueMessageCode = ConstantValue.ICC_PROCESSED;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.UNDER_FLOOR_LIMIT)) {
            valueMessageCode = ConstantValue.UNDER_FLOOR_LIMIT;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.TER_RANDOM_SEL)) {
            valueMessageCode = ConstantValue.TER_RANDOM_SEL;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.ONLINE_ICC)) {
            valueMessageCode = ConstantValue.ONLINE_ICC;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.ONLINE_CARD_ACCEPTOR)) {
            valueMessageCode = ConstantValue.ONLINE_CARD_ACCEPTOR;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.ONLINE_TERMINAL)) {
            valueMessageCode = ConstantValue.ONLINE_TERMINAL;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.ONLINE_CARD_ISSUER)) {
            valueMessageCode = ConstantValue.ONLINE_CARD_ISSUER;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.MERCHANT_SUSPICIOUS)) {
            valueMessageCode = ConstantValue.MERCHANT_SUSPICIOUS;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.FALLBACK_ICCTOMSR)) {
            valueMessageCode = ConstantValue.FALLBACK_ICCTOMSR;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.CLESS_TXN)) {
            valueMessageCode = ConstantValue.CLESS_TXN;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.CLESS_TXN_ADVICE)) {
            valueMessageCode = ConstantValue.CLESS_TXN_ADVICE;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.CUSTOMER_CANCELLATION)) {
            valueMessageCode = ConstantValue.CUSTOMER_CANCELLATION;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.UNSPECIFIED)) {
            valueMessageCode = ConstantValue.UNSPECIFIED;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.SUSPECTED_MALFUNC)) {
            valueMessageCode = ConstantValue.SUSPECTED_MALFUNC;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.FORMAT_ERROR)) {
            valueMessageCode = ConstantValue.FORMAT_ERROR;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.INCORRECT_AMOUNT)) {
            valueMessageCode = ConstantValue.INCORRECT_AMOUNT;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.RES_RECEIVED_LATE)) {
            valueMessageCode = ConstantValue.RES_RECEIVED_LATE;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.UNABLE_COMPLETE_TXN)) {
            valueMessageCode = ConstantValue.UNABLE_COMPLETE_TXN;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.UNABLE_DELIVER_MSG)) {
            valueMessageCode = ConstantValue.UNABLE_DELIVER_MSG;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.INVALID_RESP)) {
            valueMessageCode = ConstantValue.INVALID_RESP;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.TIMEOUT_WAITING_RESP)) {
            valueMessageCode = ConstantValue.TIMEOUT_WAITING_RESP;
        } else if (valueMessageCode.equalsIgnoreCase(Constant.MAC_FAILURE)) {
            valueMessageCode = ConstantValue.MAC_FAILURE;
        }
        return valueMessageCode;
    }

    /**
     * Gets the function code.
     *
     * @param valueFunctionCode
     *            the value function code
     * @return the function code
     */
    public String getFunctionCode(String valueFunctionCode) {
        String code = ConstantValue.FINANCIAL_TRANSACTION;
        if (valueFunctionCode.equalsIgnoreCase(Constant.PRE_AUTHORISATION)) {
            code = ConstantValue.AMOUNT_ESTIMATE;
        } else if (valueFunctionCode.equalsIgnoreCase(Constant.PURCHASE_ADVICE_FULL)) {
            code = ConstantValue.APPROVED_AUTH_SAME_AMOUNT;
        } else if (valueFunctionCode.equalsIgnoreCase(Constant.PURCHASE_ADVICE_MANUAL)) {
            code = ConstantValue.FINANCIAL_TRANSACTION;
        } else if (valueFunctionCode.equalsIgnoreCase(Constant.PURCHASE_ADVICE_PARTIAL)) {
            code = ConstantValue.APPROVED_AUTH_DIFF_AMOUNT;
        } else if (valueFunctionCode.equalsIgnoreCase(Constant.PRE_AUTHORISATION_VOID)) {
            code = ConstantValue.AMOUNT_ESTIMATE;
        } else if (valueFunctionCode.equalsIgnoreCase(Constant.PRE_AUTHORISATION_EXTENSION)) {
            code = ConstantValue.AUTHORIZATION_EXTENSION;
        } else if (valueFunctionCode.equalsIgnoreCase(Constant.PURCHASE_REVERSAL)) {
            code = ConstantValue.TXN_NOT_COMPLETED;
        }
        return code;
    }

    /**
     * Gets the process code.
     *
     * @param valueProcessCode
     *            the value process code
     * @return the process code
     */
    public String getProcessCode(String valueProcessCode) {
        if (valueProcessCode.equalsIgnoreCase(Constant.NOTXN_DEBIT)) {
            valueProcessCode = ConstantValue.NOTXN;
        } else if (valueProcessCode.equalsIgnoreCase(Constant.PURCHASE)) {
            valueProcessCode = ConstantValue.PURCHASE;
        } else if (valueProcessCode.equalsIgnoreCase(Constant.AUTHORIZATION_ADVICE)) {
            valueProcessCode = ConstantValue.PRE_AUTHORISATION;
        } else if (valueProcessCode.equalsIgnoreCase(Constant.FINANCIAL_ADVISE)) {
            valueProcessCode = ConstantValue.PURCHASE;
        } else if (valueProcessCode.equalsIgnoreCase(Constant.FINANCIAL_AUTHORIZATION)) {
            valueProcessCode = ConstantValue.PURCHASE;
        } else if (valueProcessCode.equalsIgnoreCase(Constant.PURCHASE_NAQD)) {
            valueProcessCode = ConstantValue.PURCHASE_NAQD;
        } else if (valueProcessCode.equalsIgnoreCase(Constant.PURCHASE_REVERSAL)) {
            valueProcessCode = ConstantValue.PURCHASE_REVERSAL;
        } else if (valueProcessCode.equalsIgnoreCase(Constant.CASH_ADVANCE)) {
            valueProcessCode = ConstantValue.CASH_ADVANCE;
        } else if (valueProcessCode.equalsIgnoreCase(Constant.REFUND)) {
            valueProcessCode = ConstantValue.REFUND;
        } else if (valueProcessCode.equalsIgnoreCase(Constant.PRE_AUTHORISATION)) {
            valueProcessCode = ConstantValue.PRE_AUTHORISATION;
        } else if (valueProcessCode.equalsIgnoreCase(Constant.PRE_AUTHORISATION_COMPLETE)) {
            valueProcessCode = ConstantValue.PRE_AUTHORISATION_COMPLETE;
        } else if (valueProcessCode.equalsIgnoreCase(Constant.PURCHASE_ADVICE_FULL)) {
            valueProcessCode = ConstantValue.PURCHASE_ADVICE_FULL;
        } else if (valueProcessCode.equalsIgnoreCase(Constant.PURCHASE_ADVICE_MANUAL)) {
            valueProcessCode = ConstantValue.PURCHASE_ADVICE_FULL;
        } else if (valueProcessCode.equalsIgnoreCase(Constant.PURCHASE_ADVICE_PARTIAL)) {
            valueProcessCode = ConstantValue.PURCHASE_ADVICE_PARTIAL;
        } else if (valueProcessCode.equalsIgnoreCase(Constant.PRE_AUTHORISATION_VOID)) {
            valueProcessCode = ConstantValue.PRE_AUTHORISATION_VOID;
        } else if (valueProcessCode.equalsIgnoreCase(Constant.PRE_AUTHORISATION_EXTENSION)) {
            valueProcessCode = ConstantValue.PRE_AUTHORISATION;
        } else if (valueProcessCode.equalsIgnoreCase(Constant.NO_OF_TXNS)) {
            valueProcessCode = ConstantValue.NO_OF_TXNS;
        } else {
            valueProcessCode = ConstantValue.PURCHASE_ADVICE_FULL;
        }
        return valueProcessCode;
    }

    /**
     * Adds the zeros.
     *
     * @param param
     *            the param
     * @param i
     *            the i
     * @return the string
     */
    public static String addZeros(String param, int i) {
        long formatParam = Long.parseLong(param);
        String format = "%0" + i + "d";
        LOGGER.info("amount -- {}", String.format(format, formatParam));
        return String.format(format, formatParam);
    }

    /**
     * Format track 2 data.
     *
     * @param track2
     *            the track 2
     * @return the string
     */
    public String formatTrack2Data(String track2) {
        if (track2 == null || track2.trim().length() == 0) {
            track2 = "";
        } else if (track2.endsWith("F")) {
            track2 = track2.substring(0, track2.length() - 1);
        } else if (37 <= track2.trim().length()) {
            track2 = (track2.substring(0, 37));
        }
        return track2;
    }

}
